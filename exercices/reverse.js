const reverse = (str) => {
  return str.split('').reduce((acc, _, index) => {
    acc+= str[str.length - (index + 1)]
    return acc
  }, '')
}

console.log(reverse("reddit"))
console.log(reverse("am-i"))

expect(reverse("reddit")).toEqual("tidder")
expect(reverse("am-i")).toEqual("am-i")