/* we prefer avoid String object, but ok for the exercice */
/* Repeat seems to already exist, so i prefer use another name to be sure mine is used  */

Object.assign(String.prototype, {
  Repeat(number) {
    let output = ""
    for(let i = 0; i < number; i ++) {
      output += this.toString()
    }
    return output
  }
})

console.log("hello".Repeat(3))
