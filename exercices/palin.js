const isPalindrome = (str) => {
  for(let i = 0; i < Math.floor(str.length / 2); i ++) {
    if(str.charAt(i) !== str.charAt(str.length - (i + 1))) {
      return false
    }
  }
  return true
}


console.log(isPalindrome('a& adsf fsda a')) // false
console.log(isPalindrome('a adsf fsda a')) // should be true
console.log(isPalindrome('kayak')) // wiki said true (odd letter count)
console.log(isPalindrome('deed')) // wiki said true (even letter count)
