

import './App.css'
import React, {useState} from "react"

function App() {

  const [users, setUsers] = useState([])
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")

  const onAddUser = (event) => {
    event.preventDefault()
    setUsers([
      ...users,
      {firstName, lastName}
      ]
    )
    setFirstName("")
    setLastName("")
  }

  const onEditLastName = (event) => {
    setLastName(event.target.value)
  }

  const onEditFirstName = (event) => {
    setFirstName(event.target.value)
  }

  return (
    <div className="App">
      <header className="App-header">
        <form onSubmit={onAddUser}>
          <label>
            First-name
            <input value={firstName} onChange={onEditFirstName} type="text"/>
          </label>
          <label>
            Last-name
            <input value={lastName} onChange={onEditLastName} type="text"/>
          </label>
          <button type="submit">Add user</button>
        </form>
        <table>
          <thead>
            <td>Index</td>
            <td>FistName</td>
            <td>LastName</td>
          </thead>
          <tbody>
            {users.map((user, index) =>
                <tr key={`${user.firstName}_${user.lastName}`}>
                  <td>{index}</td>
                  <td>{user.firstName}</td>
                  <td>{user.lastName}</td>
                </tr>
            )}
          </tbody>
        </table>
      </header>
    </div>
  )
}

export default App
